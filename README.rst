Curriculum Vitae of Andreas Hilboll
===================================

This is the LaTeX source to my current *curriculum vitae*.  It is
borrowed from `Kieran Healy's CV template
<http://kjhealy.github.io/kjh-vita/>`__, and currently depends on (a
modified version of) Kieran's `LaTeX setup
<https://github.com/kjhealy/latex-custom-kjh>`__.

